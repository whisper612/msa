package webapi

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
	"user_service/model"
	"user_service/utils"
)

const EmailQueue = "email"

type WebAPI struct {
	apiContext context.Context

	bindAddr string

	accessCollection *mongo.Collection
	userCollection   *mongo.Collection

	rabbitCh *amqp.Channel
}

func New(ctx context.Context, client *mongo.Client, rabbitCh *amqp.Channel) *WebAPI {
	api := &WebAPI{
		accessCollection: client.Database("logs").Collection("access"),
		userCollection:   client.Database("foo").Collection("users"),
		apiContext:       ctx,
		bindAddr:         "",
		rabbitCh:         rabbitCh,
	}

	_, err := api.rabbitCh.QueueDeclare(EmailQueue, true, false, false, false, nil)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %s", err.Error())
	}

	return api
}

func (api *WebAPI) ListenAndServe(ip string, port int) error {
	api.bindAddr = fmt.Sprintf("%s:%d", ip, port)
	fmt.Printf("Start server on '%s'. API is awailable at http://%s/api\n", api.bindAddr, api.bindAddr)
	return http.ListenAndServe(api.bindAddr, nil)
}

func (api *WebAPI) RegisterRoutes() {
	http.HandleFunc("/api/reg", api.handlerReg)
	http.HandleFunc("/api/login", api.handlerLogin)
	http.HandleFunc("/api/reset", api.handlerReset)
	http.HandleFunc("/api/email-confirm", api.handlerEmailConfirm)
}

func (api *WebAPI) logAccess(w http.ResponseWriter, r *http.Request) error {
	rec := model.AccessRecord{
		Method:     r.Method,
		Path:       r.URL.Path,
		RemoteAddr: r.RemoteAddr,
		Time:       time.Now().UTC(),
	}
	_, err := api.accessCollection.InsertOne(api.apiContext, rec)
	return err
}

func (api *WebAPI) simpleLogAccess(w http.ResponseWriter, r *http.Request, handlerName string) {
	if err := api.logAccess(w, r); err != nil {
		_ = fmt.Errorf("in web handler '%s' logAccess error: %s", handlerName, err.Error())
	}
}

func (api *WebAPI) handlerReg(w http.ResponseWriter, r *http.Request) {
	api.simpleLogAccess(w, r, "reg")

	r.ParseForm()
	username := r.Form.Get("username")
	password := r.Form.Get("password")
	email := r.Form.Get("email")

	if username == "" || password == "" || email == "" {
		http.Error(w, "Arguments is empty", http.StatusBadRequest)
		return
	}
	// проверка на существующего пользователя
	filter := bson.M{
		"$or": []interface{}{
			bson.M{"username": username},
			bson.M{"email": email},
		},
	}
	res := api.userCollection.FindOne(api.apiContext, filter)
	if res.Err() == nil {
		http.Error(w, "Username or email is busy", http.StatusNotFound)
		return
	}

	salt := strconv.Itoa(rand.Int())
	hash := utils.GetPasswordHash(password, salt)
	user := model.User{
		ID:           primitive.NewObjectID(),
		Username:     username,
		PasswordHash: hash,
		PasswordSalt: salt,
		Email:        email,
		IsConfirmed:  false,
	}

	inserted, err := api.userCollection.InsertOne(api.apiContext, user)
	if err != nil {
		fmt.Errorf("fail DB insert of registration: %s", err.Error())
	}
	fmt.Printf("new user[%s]: %s", inserted.InsertedID, user.Username)

	userIDHex := inserted.InsertedID.(primitive.ObjectID).Hex()

	emailBody, err := json.Marshal(model.EmailRequest{
		DestEmail:   user.Email,
		SourceEmail: "lol@kek.com",
		Body:        fmt.Sprintf("To confirm the email, go to http://%s/api/email-confirm?id=%s", api.bindAddr, userIDHex),
	})

	if err != nil {
		fmt.Errorf("marshal email request failed: %s", err.Error())
	}

	err = api.rabbitCh.Publish("", EmailQueue, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "text/json",
		Body:         emailBody,
	})
	if err != nil {
		fmt.Errorf("publish email request failed: %s", err.Error())
	}

	w.Write([]byte("See email inbox"))
}

func (api *WebAPI) handlerLogin(w http.ResponseWriter, r *http.Request) {
	api.simpleLogAccess(w, r, "login")

	r.ParseForm()
	username := r.Form.Get("username")
	password := r.Form.Get("password")
	if password == "" {
		http.Error(w, "Argument is empty", http.StatusBadRequest)
		return
	}

	var user model.User

	err := api.userCollection.FindOne(api.apiContext, bson.D{{"username", username}}).Decode(&user)
	if err != nil {
		http.Error(w, "User is undefined", http.StatusNotFound)
		return
	}

	tmpHash := utils.GetPasswordHash(password, user.PasswordSalt)

	if tmpHash == user.PasswordHash {
		w.Write([]byte("OK"))
	} else {
		http.Error(w, "Password is wrong", http.StatusNotFound)
		return
	}
}

func (api *WebAPI) handlerReset(w http.ResponseWriter, r *http.Request) {
	api.simpleLogAccess(w, r, "reset")

	r.ParseForm()
	username := r.Form.Get("username")
	email := r.Form.Get("email")

	if username == "" && email == "" {
		http.Error(w, "Arguments is empty", http.StatusBadRequest)
		return
	}
	// проверка на существующего пользователя
	filter := bson.M{
		"$or": []interface{}{
			bson.M{"username": username},
			bson.M{"email": email},
		},
	}
	res := api.userCollection.FindOne(api.apiContext, filter)
	if res.Err() != nil {
		http.Error(w, "Username or email is undefined", http.StatusNotFound)
		return
	}

	w.Write([]byte("Пароль сброшен"))
}

func (api *WebAPI) handlerEmailConfirm(w http.ResponseWriter, r *http.Request) {
	api.simpleLogAccess(w, r, "email confirm")

	userID, err := primitive.ObjectIDFromHex(r.URL.Query().Get("id"))
	if err != nil {
		http.Error(w, "Invalid id", http.StatusBadRequest)
		return
	}

	_, err = api.userCollection.UpdateOne(
		api.apiContext,
		bson.M{"_id": userID},
		bson.D{{
			"$set", bson.D{{
				"is_confirmed",
				true,
			}},
		}},
	)

	if err != nil {
		http.Error(w, "Confirm failed", http.StatusInternalServerError)
		fmt.Errorf("Confirm failed: %v", err.Error())
		return
	}

	w.Write([]byte("Confirmed"))
}
