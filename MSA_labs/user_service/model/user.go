package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID           primitive.ObjectID `bson:"_id"`
	Username     string             `bson:"username"`
	PasswordHash string             `bson:"password_hash"`
	PasswordSalt string             `bson:"password_salt"`
	Email        string             `bson:"email"`
	IsConfirmed  bool               `bson:"is_confirmed"`
}
