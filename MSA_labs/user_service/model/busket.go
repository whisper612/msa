package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type BasketItem struct {
	ItemId primitive.ObjectID `bson:"item_id"`
	Count  int                `bson:"count"`
}

type Basket struct {
	UserID   primitive.ObjectID `bson:"_user_id"`
	ItemList []BasketItem       `bson:"item_list"`
}
