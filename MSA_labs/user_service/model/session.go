package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type SessionToken struct {
	ID          primitive.ObjectID `bson:"_id"`
	UserID      primitive.ObjectID `bson:"_user_id"`
	BeginTime   time.Time          `bson:"begin_time"`
	ExpiredTime time.Time          `bson:"expired_time"`
}
