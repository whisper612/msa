package model

type EmailRequest struct {
	DestEmail   string `json:"dest_email"`
	SourceEmail string `json:"source_email"`
	Body        string `json:"body"`
}
