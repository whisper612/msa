package main

import (
	"context"
	"github.com/streadway/amqp"
	"log"

	"user_service/webapi"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var ctx = context.TODO()

func GetDataBase() (*mongo.Client, error) {
	clientOptions := options.Client().ApplyURI("mongodb://root:example@mongo-db:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func GetRabbitMQ() (*amqp.Connection, error) {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func main() {

	db, err := GetDataBase()
	if err != nil {
		log.Fatalf("Get database error: %s", err.Error())
	}

	conn, err := GetRabbitMQ()
	if err != nil {
		log.Fatalf("Get RabbitMQ error: %s", err.Error())
	}
	defer conn.Close()

	channel, err := conn.Channel()
	if err != nil {
		log.Fatalf("Get RabbitMQ channel error: %s", err.Error())
	}
	defer channel.Close()

	api := webapi.New(ctx, db, channel)
	api.RegisterRoutes()
	log.Fatal(api.ListenAndServe("0.0.0.0", 8080))
}
