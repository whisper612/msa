package utils

import (
	"crypto/sha256"
	"encoding/hex"
)

func GetPasswordHash(password string, salt string) string {
	hashAlgo := sha256.New()
	hashAlgo.Write([]byte(password + salt))
	hash := hex.EncodeToString(hashAlgo.Sum(nil))

	return hash
}
