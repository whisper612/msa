package main

import (
	"catalog_service/webapi"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

var ctx = context.TODO()

func GetDataBase() (*mongo.Client, error) {
	clientOptions := options.Client().ApplyURI("mongodb://root:example@mongo-db:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func main() {

	db, err := GetDataBase()
	if err != nil {
		log.Fatalf("Get database error: %s", err.Error())
	}

	api := webapi.New(ctx, db)
	api.RegisterRoutes()
	log.Fatal(api.ListenAndServe("0.0.0.0", 7080))
}
