package model

import "time"

type AccessRecord struct {
	Method     string    `bson:"method"`
	Path       string    `bson:"path"`
	RemoteAddr string    `bson:"remote_addr"`
	Time       time.Time `bson:"time"`
}
