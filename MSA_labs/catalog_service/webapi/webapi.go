package webapi

import (
	"catalog_service/model"
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"time"
)

type WebAPI struct {
	apiContext context.Context

	accessCollection     *mongo.Collection
	categoriesCollection *mongo.Collection
	itemsCollection      *mongo.Collection
}

func New(ctx context.Context, client *mongo.Client) *WebAPI {
	return &WebAPI{
		accessCollection: client.Database("logs").Collection("access"),
		categoriesCollection: client.Database("shop").Collection("categories"),
		itemsCollection: client.Database("shop").Collection("items"),
		apiContext: ctx,
	}
}

func (api *WebAPI) ListenAndServe(ip string, port int) error {
	addr := fmt.Sprintf("%s:%d", ip, port)
	fmt.Printf("Start server on '%s'. API is awailable at http://%s/api\n", addr, addr)
	return http.ListenAndServe(addr, nil)
}

func (api *WebAPI) RegisterRoutes() {
	http.HandleFunc("/api/categories", api.handlerCategories)
	http.HandleFunc("/api/list", api.handlerList)
	http.HandleFunc("/api/item", api.handlerItem)
}

func (api *WebAPI) logAccess(w http.ResponseWriter, r *http.Request) error {
	rec := model.AccessRecord{
		Method:		r.Method,
		Path:       r.URL.Path,
		RemoteAddr: r.RemoteAddr,
		Time:       time.Now().UTC(),
	}
	_, err := api.accessCollection.InsertOne(api.apiContext, rec)
	return err
}

func (api *WebAPI) handlerCategories(w http.ResponseWriter, r *http.Request) {
	if err := api.logAccess(w, r); err != nil {
		fmt.Errorf("handlerReg logAccess error: %s", err.Error())
	}

	cursor, err := api.categoriesCollection.Find(api.apiContext, bson.M{})
	if err != nil {
		http.Error(w, "Category not found", http.StatusNotFound)
		return
	}

	var categoriesData []model.Category
	err = cursor.All(api.apiContext, &categoriesData)
	if err != nil {
		fmt.Errorf("fail to cursor.All: %s", err.Error())
		return
	}

	data, _ := json.Marshal(categoriesData)
	w.Write(data)
}

func (api *WebAPI) handlerList(w http.ResponseWriter, r *http.Request) {
	if err := api.logAccess(w, r); err != nil {
		fmt.Errorf("handlerReg logAccess error: %s", err.Error())
	}

	r.ParseForm()
	categoryID, err := primitive.ObjectIDFromHex(r.Form.Get("category_id"))
	if err != nil {
		fmt.Errorf("category id for list recieving error: %s", err.Error())
		return
	}

	cursor, err := api.itemsCollection.Find(api.apiContext, bson.M{ "category_id": categoryID })
	if err != nil {
		http.Error(w, "Category not found", http.StatusNotFound)
		return
	}

	var items []model.Item
	err = cursor.All(api.apiContext, &items)
	if err != nil {
		fmt.Errorf("fail to cursor.All: %s", err.Error())
		return
	}

	data, _ := json.Marshal(items)
	w.Write(data)
}

func (api *WebAPI) handlerItem(w http.ResponseWriter, r *http.Request) {
	if err := api.logAccess(w, r); err != nil {
		fmt.Errorf("handlerReg logAccess error: %s", err.Error())
	}

	r.ParseForm()
	itemID, err := primitive.ObjectIDFromHex(r.Form.Get("_id"))
	if err != nil {
		http.Error(w, fmt.Sprintf("Arguments error: %s", err.Error()), http.StatusBadRequest)
		return
	}

	var item model.Item
	err = api.itemsCollection.FindOne(api.apiContext, bson.D{{"_id", itemID}}).Decode(&item)
	if err != nil {
		http.Error(w, "Item ID is undefined", http.StatusNotFound)
		return
	}

	data, _ := json.Marshal(item)
	w.Write(data)
}