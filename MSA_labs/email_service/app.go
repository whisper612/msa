package main

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/streadway/amqp"
)

const (
	AMQPConnectionURL = "amqp://guest:guest@rabbitmq:5672/"
)

type EmailRequest struct {
	DestEmail   string `json:"dest_email"`
	SourceEmail string `json:"source_email"`
	Body        string `json:"body"`
}

func handleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial(AMQPConnectionURL)
	handleError(err, "Can't connect to AMQP")
	defer conn.Close()

	amqpChannel, err := conn.Channel()
	handleError(err, "Can't create a amqpChannel")

	defer amqpChannel.Close()

	queue, err := amqpChannel.QueueDeclare("email", true, false, false, false, nil)
	handleError(err, "Could not declare `email` queue")

	err = amqpChannel.Qos(1, 0, false)
	handleError(err, "Could not configure QoS")

	messageChannel, err := amqpChannel.Consume(
		queue.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	handleError(err, "Could not register consumer")

	log.Printf("Consumer ready, PID: %d", os.Getpid())
	for d := range messageChannel {
		log.Printf("Received a message: %s", d.Body)

		var emailReq EmailRequest

		err := json.Unmarshal(d.Body, &emailReq)

		if err != nil {
			log.Printf("Error decoding JSON: %s", err)
		}

		time.AfterFunc(time.Second, func() {
			log.Printf("Email sended to '%s' from '%s' with body: %s", emailReq.DestEmail, emailReq.SourceEmail, emailReq.Body)

			if err := d.Ack(false); err != nil {
				log.Printf("Error acknowledging message : %s", err)
			} else {
				log.Printf("Acknowledged message")
			}
		})
	}
}
